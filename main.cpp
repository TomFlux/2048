// 	g++ -o YourProgName YourSource.cpp -lX11 -lGL -lpthread -lpng -lstdc++fs -std=c++17
#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include <string>
#include <map>
#include <vector>

int WIDTH  = 400;
int HEIGHT = 400;

std::map<int, std::vector<int>> COLOURS = {
        {2, {224, 224, 226}},
        {4, {129, 210, 199}},
        {8, {181, 186, 208}},
        {16, {115, 137, 174}},
        {32, {65, 103, 136}}
};

class Tile
{
public:
    bool moved = false;
    bool moving = false;
    bool merged = false;
    int value = 0;
    int dir[2] = {0,0}; // x, y
};

class Board : public olc::PixelGameEngine
{
public:
    Tile tiles[4][4] = {Tile()};

    std::vector<std::vector<int>> free_tiles = {};

    bool game_over = false;

    int unit_size;
    int score = 0;
    int frees_count = 0;

    Board()
    {
        sAppName = "2048";
    }

public:
    bool OnUserCreate() override
    {
        unit_size = std::min(WIDTH / 4, HEIGHT / 4) / 2;
        gen_new_tiles();
        return true;
    }

    bool OnUserUpdate(float fElapsedTime) override
    {
        if (game_over) return true;

        if (handle_movement()) {
            count_frees(); // used for tile generation
            gen_new_tiles();
            check_gameover();
        }

        draw_screen();

        return true;
    }
private:
    void count_frees() {
        frees_count = 0;
        free_tiles.clear();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (tiles[i][j].value == 0) {
                    frees_count ++;

                    free_tiles.push_back({i,j});
                }
            }
        }
    }

    void gen_new_tiles() {
       int new_tiles = std::min(3, frees_count); // don't generate more tiles than are free

       for (int _ = 0; _ < new_tiles; _ ++) {
           if ((rand() % 10) > 2) {
                int new_index = rand() % free_tiles.size();
                std::vector<int> new_coords = free_tiles[new_index];

                int new_value;
                if ((rand() % 10) > 5) {
                    new_value = 2;
                } else {
                    new_value = 4;
                }

                Tile new_tile = Tile();
                new_tile.value = new_value;

                tiles[new_coords[0]][new_coords[1]] = new_tile;

                free_tiles.erase(free_tiles.begin() + new_index);
           }
       }
    }

    void check_gameover() {
        /* Perform move check for every tile:
         *  - one tile checks in all directions and stores bools in vector
         *  - each tile then & all checks into "moveable" then store in vector
         *  - each elm of the vector is also & together
         */

        if (frees_count != 0) return; // game can't be over if there are still spaces

        std::vector<bool> checks = {};
        for (int i = 0; i < 4; i ++) {
            for (int j = 0; j < 4; j ++) {
                Tile this_tile = tiles[i][j];
                bool moveable = true;

                if (this_tile.value == 0) continue;

                if (i - 1 != 0) {
                    moveable &= tiles[i - 1][j].value != 0 && this_tile.value == tiles[i - 1][j].value;
                }
                if (i + 1 != 4) {
                    moveable &= tiles[i + 1][j].value != 0 && this_tile.value == tiles[i + 1][j].value;
                }
                if (j - 1 != 0) {
                    moveable &= tiles[i][j - 1].value != 0 && this_tile.value == tiles[i][j - 1].value;
                }
                if (j + 1 != 4) {
                    moveable &= tiles[i][j + 1].value != 0 && this_tile.value == tiles[i][j + 1].value;
                }

                checks.push_back(moveable);
            }
        }

        game_over = true;

        for (size_t i = 0; i < checks.size(); i++) {
            game_over &= !checks[i];
        }

        std::cout << "Game over: " << game_over << std::endl;

    }

    void draw_screen() {
        FillRect(0, 0, WIDTH, HEIGHT, olc::BLACK); // clear screen
        DrawString(0, 0, std::string("Score: " + std::to_string(score)), olc::WHITE);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                int x =  (i * unit_size) + WIDTH / 4;
                int y =  (j * unit_size) + HEIGHT / 4;

                std::vector<int> colour = {254,254,254};

                if (COLOURS.find(tiles[i][j].value) != COLOURS.end()) {
                    colour = COLOURS[tiles[i][j].value];
                } else if (tiles[i][j].value != 0){
                    colour = COLOURS[32];
                }

                FillRect(x, y, unit_size - 10, unit_size - 10, olc::Pixel(colour[0], colour[1], colour[2]));

                std::string coords;
                switch (tiles[i][j].value) {
                    case 0:
                        coords = "";
                        break;
                    default:
                        coords = std::string(std::to_string(tiles[i][j].value));
                }

                DrawString(x + 5, y + 5, coords, olc::BLACK);
                if (game_over) DrawString((WIDTH / 2) - 40, HEIGHT - 10, "GAME OVER!", olc::RED);
            }
        }
    }
    bool handle_movement() {
        bool key_down = false;
        int h_look = 0;
        int v_look = 0;

        if (GetKey(olc::Key::LEFT).bPressed) {
            h_look = -1;
            key_down = true;
        }
        else if (GetKey(olc::Key::RIGHT).bPressed) {
            h_look = 1;
            key_down = true;
        }
        else if (GetKey(olc::Key::UP).bPressed) {
            v_look = -1;
            key_down = true;
        }
        else if (GetKey(olc::Key::DOWN).bPressed) {
            v_look = 1;
            key_down = true;
        }

        if (key_down) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    Tile this_tile = tiles[i][j];
                    if (!this_tile.moved && this_tile.value != 0) {

                        if ((i + h_look) != 4 && (i + h_look) != -1 && (j + v_look) != 4 && (j + v_look) != -1) { // bounds check
                            // setup some variables to simplify code
                            int curr_i = i;
                            int next_i = i + h_look;
                            int curr_j = j;
                            int next_j = j + v_look;

                            while (next_i != 4 && next_i != -1 && next_j != 4 && next_j != -1) {
                                Tile next_tile = tiles[next_i][next_j];
                                Tile curr_tile = tiles[curr_i][curr_j];

                                // merge
                                if (this_tile.value == next_tile.value && !next_tile.merged) {
                                    tiles[next_i][next_j].value *= 2;
                                    score += tiles[next_i][next_j].value;
                                    tiles[curr_i][curr_j].value = 0;
                                    tiles[next_i][next_j].merged = true;

                                // move
                                } else if (next_tile.value == 0) {
                                    curr_tile.moved = true;
                                    tiles[next_i][next_j] = curr_tile;
                                    tiles[curr_i][curr_j] = Tile();

                                    curr_i += h_look;
                                    next_i = curr_i + h_look;
                                    curr_j += v_look;
                                    next_j = curr_j + v_look;

                                // tile can't be moved
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        // reset the board
        for (auto & tile_row : tiles) {
            for (auto & tile : tile_row) {
                tile.moved = false;
                tile.merged = false;
            }
        }

        return key_down;
    }
};


int main()
{
    srand(time(NULL));
    Board board;
    if (board.Construct(WIDTH + 10, HEIGHT + 10, 4, 4)) {
        board.Start();
    }

    return 0;
}
